#lang typed/racket

;; Better, but still...

;; bitmap3.rkt:11:14: Type Checker: send: method not understood by object
;;   method name: get-width
;;   object type: (Object)
;;   in: (send bitmap get-width)
;;   location...:
;;    bitmap3.rkt:11:14
;;   context...:
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/utils/tc-utils.rkt:123:0: report-all-errors
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typecheck/tc-toplevel.rkt:376:0: type-check
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typecheck/tc-toplevel.rkt:619:0: tc-module
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/tc-setup.rkt:94:0: tc-module/full
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typed-racket.rkt:23:4: module-begin
;; bitmap3.rkt:12:15: Type Checker: send: method not understood by object
;;   method name: get-height
;;   object type: (Object)
;;   in: (send bitmap get-height)
;;   location...:
;;    bitmap3.rkt:12:15
;;   context...:
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/utils/tc-utils.rkt:133:21: for-loop
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/utils/tc-utils.rkt:123:0: report-all-errors
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typecheck/tc-toplevel.rkt:376:0: type-check
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typecheck/tc-toplevel.rkt:619:0: tc-module
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/tc-setup.rkt:94:0: tc-module/full
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typed-racket.rkt:23:4: module-begin
;; bitmap3.rkt:10:0: Type Checker: send: method not understood by object
;;   method name: load-file
;;   object type: (Object)
;;   in: (send bitmap load-file "test.png")
;;   location...:
;;    bitmap3.rkt:10:0
;;   context...:
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/utils/tc-utils.rkt:133:21: for-loop
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/utils/tc-utils.rkt:123:0: report-all-errors
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typecheck/tc-toplevel.rkt:376:0: type-check
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typecheck/tc-toplevel.rkt:619:0: tc-module
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/tc-setup.rkt:94:0: tc-module/full
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typed-racket.rkt:23:4: module-begin
;; bitmap3.rkt:16:0: Type Checker: send: method not understood by object
;;   method name: get-argb-pixels
;;   object type: (Object)
;;   in: (send bitmap get-argb-pixels 0 0 width height argb)
;;   location...:
;;    bitmap3.rkt:16:0
;;   context...:
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/utils/tc-utils.rkt:133:21: for-loop
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/utils/tc-utils.rkt:123:0: report-all-errors
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typecheck/tc-toplevel.rkt:376:0: type-check
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typecheck/tc-toplevel.rkt:619:0: tc-module
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/tc-setup.rkt:94:0: tc-module/full
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typed-racket.rkt:23:4: module-begin
;; Type Checker: Summary: 4 errors encountered
;;   location...:
;;    bitmap3.rkt:11:14
;;    bitmap3.rkt:12:15
;;    bitmap3.rkt:10:0
;;    bitmap3.rkt:16:0
;;   context...:
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typecheck/tc-toplevel.rkt:376:0: type-check
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typecheck/tc-toplevel.rkt:619:0: tc-module
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/tc-setup.rkt:94:0: tc-module/full
;;    /usr/share/racket/pkgs/typed-racket-lib/typed-racket/typed-racket.rkt:23:4: module-begin

(require/typed
    racket/draw
  (make-bitmap
   (-> Fixnum Fixnum
       (Object))))

(define bitmap (make-bitmap 10 10))
(send bitmap load-file "test.png")
(define width (send bitmap get-width))
(define height (send bitmap get-height))
(displayln (format "bitmap ~ax~a" width height))
(define num-pixels (* width height))
(define argb (make-bytes (* num-pixels 4)))
(send bitmap get-argb-pixels 0 0 width height argb)
(writeln argb)
