#lang racket

(provide wrap-struct)

(define-syntax (wrap-struct stx)
  (syntax-case stx ()
    ((_ name (fields ...) options ...)
     (with-syntax ((name-fields (datum->syntax
                                 stx
                                 (string->symbol
                                  (format "~a-fields" (syntax->datum #'name))))))
       #'(begin
           (struct name (fields ...) options ...)
           (define name-fields (quote (fields ...))))))))
