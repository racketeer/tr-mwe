#lang racket

(require "syntax1-struct.rkt")

(wrap-struct test-struct (a b c) #:transparent)

(define my-struct (test-struct 1 2 3))
(displayln my-struct)
(displayln test-struct-fields)
