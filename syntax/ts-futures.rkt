#lang typed/racket/base

(require racket/future
         (for-syntax racket/base)
         racket/fixnum)

(provide define-futurized)

(define futures-depth
  (make-parameter
   (inexact->exact (ceiling (/ (log (exact->inexact (processor-count)))
                               (log 2.0))))))

(define-syntax (define-futurized stx)
  (syntax-case stx ()
    ((_ (proc (start :1 stype) (end :2 etype)) ::: RetType body ...)
     #'(begin
         (define max-depth (futures-depth))
         (define (proc (start : stype)
                       (end : etype)
                       (depth : Integer 0)) : RetType
           (cond ((fx< depth max-depth)
                  (define mid (fx+ start (fxrshift (fx- end start) 1)))
                  (let ((f (future
                            (λ ()
                              (proc start mid (fx+ depth 1))))))
                    (proc mid end (fx+ depth 1))
                    (touch f)))
                 (else
                  body ...)))))))
